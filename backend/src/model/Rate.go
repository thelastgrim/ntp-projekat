package model

type Rate struct {
	Id      string `json:"id"`
	MovieId string `json:"movieId"`
	Mark    int    `json:"mark"`
}
