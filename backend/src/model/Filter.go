package model

import "time"

type Filter struct {
	Name                 string    `json:"name"`
	Genre                string    `json:"genre"`
	Description          string    `json:"description"`
	Language             string    `json:"language"`
	Director             string    `json:"director"`
	NumOfAwards          int       `json:"numOfAwards"`
	MainActors           string    `json:"mainActors"`
	MovieLengthFrom      int       `json:"movieLengthFrom"`
	MovieLengthTo        int       `json:"movieLengthTo"`
	YearOfProductionFrom time.Time `json:"yearOfProductionFrom"`
	YearOfProductionTo   time.Time `json:"yearOfProductionTo"`
	BudgetFrom           int       `json:"budgetFrom"`
	BudgetTo             int       `json:"budgetTo"`
	SortBy               string    `json:"sortBy"`
}
