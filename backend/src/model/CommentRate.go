package model

type CommentRate struct {
	Id        string `json:"id"`
	CommentId string `json:"commentId"`
	Mark      int    `json:"mark"`
}
