package repository

import (
	"database/sql"
	"fmt"
	"model"

	_ "github.com/lib/pq"
)

type CommentLikeRepository interface {
	FindAllByCommentId(movieId string) ([]model.CommentLike, error)
	Save(comment *model.CommentLike) (*model.CommentLike, error)
}

type commentLikeRepo struct{}

func NewCommentLikeRepository() CommentLikeRepository {
	return &commentLikeRepo{}
}

func (*commentLikeRepo) FindAllByCommentId(commentId string) ([]model.CommentLike, error) {
	dbConnetion := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err := sql.Open("postgres", dbConnetion)
	CheckError(err)

	rows, err := db.Query(`SELECT "id", "commentId", "like" FROM "CommentLike" WHERE "commentId" = $1`, commentId)
	CheckError(err)

	// var movies []model.Movie
	likes := []model.CommentLike{}

	for rows.Next() {
		var id string
		var commentId string
		var like int

		err = rows.Scan(&id, &commentId, &like)
		CheckError(err)

		likes = append(likes, model.CommentLike{Id: id, CommentId: commentId, Like: like})
	}

	defer db.Close()

	defer rows.Close()

	return likes, nil
}

func (*commentLikeRepo) Save(commentLike *model.CommentLike) (*model.CommentLike, error) {
	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)

	db, err := sql.Open("postgres", psqlconn)
	CheckError(err)

	insertStmt := `insert into "CommentLike"("id", "commentId", "like") values($1, $2, $3)`
	_, e := db.Exec(insertStmt, commentLike.Id, commentLike.CommentId, commentLike.Like)
	CheckError(e)

	defer db.Close()

	return commentLike, nil
}
