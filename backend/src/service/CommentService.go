package service

import (
	"errors"
	"model"
	"repository"

	"github.com/google/uuid"
)

type CommentService interface {
	Validate(comment *model.Comment) error
	ValidateLike(like *model.CommentLike) error
	ValidateRate(like *model.CommentRate) error
	Save(comment *model.Comment) (*model.Comment, error)
	SaveLike(like *model.CommentLike) (*model.CommentLike, error)
	SaveRate(like *model.CommentRate) (*model.CommentRate, error)
}

type commentService struct{}

var (
	commentRepo     repository.CommentRepository     = repository.NewCommentRepository()
	commentLikeRepo repository.CommentLikeRepository = repository.NewCommentLikeRepository()
	commentRateRepo repository.CommentRateRepository = repository.NewCommentRateRepository()
)

func NewCommentService() CommentService {
	return &commentService{}
}

func (*commentService) Save(comment *model.Comment) (*model.Comment, error) {

	comment.Id = uuid.New().String()

	return commentRepo.Save(comment)
}

func (*commentService) SaveLike(commentLike *model.CommentLike) (*model.CommentLike, error) {

	commentLike.Id = uuid.New().String()

	return commentLikeRepo.Save(commentLike)
}

func (*commentService) SaveRate(commentRate *model.CommentRate) (*model.CommentRate, error) {

	commentRate.Id = uuid.New().String()

	return commentRateRepo.Save(commentRate)
}

func (*commentService) Validate(comment *model.Comment) error {

	if comment.MovieId == "" {
		error := errors.New("Comment MovieId cannot be empty.")
		return error
	}

	if comment.Content == "" {
		error := errors.New("Comment Content cannot be empty.")
		return error
	}

	return nil
}

func (*commentService) ValidateLike(like *model.CommentLike) error {

	if like.CommentId == "" {
		error := errors.New("Comment MovieId cannot be empty.")
		return error
	}

	if like.Like < 0 {
		error := errors.New("Like cannot be les than 0.")
		return error
	}

	if like.Like > 1 {
		error := errors.New("Like cannot be greater than 1.")
		return error
	}

	return nil
}

func (*commentService) ValidateRate(rate *model.CommentRate) error {

	if rate.CommentId == "" {
		error := errors.New("Comment MovieId cannot be empty.")
		return error
	}

	if rate.Mark < 1 {
		error := errors.New("Mark cannot be les than 1.")
		return error
	}

	if rate.Mark > 5 {
		error := errors.New("Mark cannot be greater than 5.")
		return error
	}

	return nil
}
