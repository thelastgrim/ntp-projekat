package service

import (
	"model"
	"repository"

	_ "github.com/lib/pq"
)

type MovieService interface {
	FindAll() ([]model.Movie, error)
	FindAllFilter(filter *model.Filter) ([]model.Movie, error)
}

type movieService struct{}

var (
	movieRepo repository.MovieRepository = repository.NewMovieRepository()
)

func NewMovieService() MovieService {
	return &movieService{}
}

func (*movieService) FindAll() ([]model.Movie, error) {

	return movieRepo.FindAll()
}

func (*movieService) FindAllFilter(filter *model.Filter) ([]model.Movie, error) {

	return movieRepo.FindAllFilter(filter)
}
