# IMDB - Find your movie

Samostalno definisani projekat iz predmeta NTP.

## Opis aplikacije

Korisnici mogu da pregledaju, sortiraju, filtriraju filmove, da komentarišu i daju ocene filmovima. Filtriranje filmova je na osnovu atributa kao sto su žanr filma, glumci, godina snimanja, opis filma i drugih. Sortiranje filmova je na osnovu godine proizvodnje i ukupnog troška za snimanje filma. Korisnici takodje mogu i da lajkuju i ocenjuju komentare, kao i da komentarisu komentare. 

## Tehnologije

Pharo koristim za razvoj frontend-a, Golang za razvoj backend-a i za bazu podataka koristim Postgress.
